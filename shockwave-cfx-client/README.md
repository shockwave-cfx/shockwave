shockwave-cfx-client
====================

Client module of ShockWave Racing. Written in JavaScript.

Development
-----------
Start FXServer first.

npm install
npm start

This will install dependencies and restart the resource automatically when
changes are made.

Modules
-------

Modules that are not tied to the gamemode can be found in /modules. The goal is
to share those modules with different resources, but also release as a
standalone version on npm.

import { RespawnPlayer } from './spawn';

AddEventHandler('onClientResourceStart', async (resourceName) => {
  if (resourceName !== GetCurrentResourceName()) {
    return;
  }

  // World
  SetCloudHatOpacity(0);
  // SetWeatherTypeNowPersist('EXTRASUNNY');
  // setInterval(() => NetworkOverrideClockTime(10, 0, 0), 500);

  // Player
  StatSetInt('MP0_STAMINA', 100);

  if (GetIsLoadingScreenActive() || IsEntityDead(PlayerPedId())) {
    await RespawnPlayer('ig_bankman');
    await FadeScreenIn(500);
  }
});

// -----------------------------------------------------------------------------

AddEventHandler('onClientPlayerWasted', () => {
  setTimeout(async () => {
    await FadeScreenOut(500);
    await RespawnPlayer('ig_bankman');
    await FadeScreenIn(500);
  }, 2500);
});

// -----------------------------------------------------------------------------

AddEventHandler('onClientPlayerWasted', () => {
  const [posX, posY, posZ] = GetEntityCoords(PlayerPedId());
  const deathMarker = AddBlipForCoord(posX, posY, posZ);

  SetBlipSprite(deathMarker, 274);
});

import React from 'react';
import Root from '../components/core/Root';
import Bottom from '../components/elements/Bottom';
import LoadingSpinner from '../components/spinner/LoadingSpinner';
import GameProgress from '../containers/GameProgress';

export default () => (
  <Root>
    <Bottom>
      <LoadingSpinner />
      <GameProgress />
    </Bottom>
  </Root>
);

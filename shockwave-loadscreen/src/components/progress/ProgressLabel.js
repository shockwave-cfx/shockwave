import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Component = ({ className, message }) => (
  <div className={className}>
    {message}
  </div>
);

Component.propTypes = {
  className: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
};

export default styled(Component)`
  width: 100%;
  padding: 8px 12px;
  color: #7a7e82;
`;

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const width = ({ progress }) => `${progress * 100}%`;

const Bar = styled.div`
  width: ${width};
  height: 100%;
  background-color: #7a7e82;
`;

const Component = ({ className, progress }) => (
  <div className={className}>
    <Bar progress={progress} />
  </div>
);

Component.propTypes = {
  className: PropTypes.string.isRequired,
  progress: PropTypes.number.isRequired
};

export default styled(Component)`
  width: 100%;
  height: 4px;
`;

import styled from 'styled-components';

export default styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
`;

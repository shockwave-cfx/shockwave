import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SpinnerStripe from './SpinnerStripe';

const Component = ({ className }) => (
  <div className={className}>
    <SpinnerStripe delay="-1.2s" />
    <SpinnerStripe delay="-1.1s" />
    <SpinnerStripe delay="-1.0s" />
    <SpinnerStripe delay="-0.9s" />
    <SpinnerStripe delay="-0.8s" />
  </div>
);

Component.propTypes = {
  className: PropTypes.string.isRequired
};

export default styled(Component)`
  width: 100%;
  height: 125px;
  padding-top: 50px;
  padding-bottom: 50px;
  text-align: center;
  font-size: 10px;
`;

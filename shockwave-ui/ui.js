const ui = document.querySelector('.ui');

window.addEventListener('message', (event) => {
  if (event.data.command === 'show-ui') {
    ui.classList.remove('hidden');
  } else if (event.data.command === 'hide-ui') {
    ui.classList.add('hidden');
  }
});
